# Backend Challenge

Are you prepared?

## Description

We want to understand you better, so do not be afraid of not completing the entire test, or failing at all, this is not a right or wrong matter. Just do your best. Good luck!

## Table of Contents

- [Goal](#goal)
- [Recommendations](#recommendations)
- [User Stories](#user-stories)
- [Business Rules](#business-rules)
- [Deliverables](#deliverables)
- [Evaluation](#evaluation)
- [Bonus Deliverable](#bonus-deliverable)
- [Questions?](#questions)

## Goal

Your team is developing an application for users to create and explore tourist spots on a map. 
Your task is to create a RESTful API for the app to consume and register this information.


## Recommendations

You may choose:
* your favorite language.
* your favorite database.
* to use a framework or not.
* any piece of technology you think is necessary or cool.

It must:
* implement tests.
* be easy to setup and deploy.

Small commits make it easier for us to understand the evolution of your line of reasoning.   


## User Stories
As a anonymous user I want to:
* signup / signin with email and password.
* (bonus) signup / signin with Facebook.

As a logged in user, I want to:
* see a list of tourist spots 
* search for tourist spots by name.
* register a tourist spot (picture, name, geographical location and category).
* add pictures to a tourist spot.
* remove pictures I added to a tourist spot.
* favorite a tourist spot.
* see my favorites tourist spots.
* remove a tourist spot from my favorites.
* create new categories.
* (bonus) see a list of tourist spots in a 5 km radius from a given location.


### Business Rules

* Anonymous users can only see things.
* Initial Categories are "Park", "Museum", "Theater", "Monument"

## Deliverables

* The source code in a public git repository.
* Provide a public working environment (eg. Heroku).
* An Swagger/OpenAPI documentation.
* Collections from Postman
* Instructions how to run the development environment.
* Instructions how to deploy.


## Evaluation

The evaluation will follow the criteria below.

* Good practices.
* Code maintainability.
* Performance.
* Privacy.
* Data security.
* Data consistency.
* Full operation.
* Reliability.
* Robustness.
* Scalability.


## Bonus Deliverable

Beyond your hard skills we are also interested on how you think, the way your code evolves and grows until it's done. 
So if you are feeling like a [awesome developer streamer](https://github.com/bnb/awesome-developer-streams) :), 
we would like to see a video of your development process. It would allow us to better understand you and give you a 
more complete feedback.


## Questions?

If you have any question about the challenge, we have a discussing forum for you!

[Dev Challenge Forum](https://groups.google.com/a/snowmanlabs.com/forum/#!forum/dev.challenge).

**Enjoy**